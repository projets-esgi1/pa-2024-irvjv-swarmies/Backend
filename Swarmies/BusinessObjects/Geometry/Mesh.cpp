//
// Created by ivo on 01/11/2023.
//

#include <iostream>

#include "Mesh.hpp"

namespace Swarmies {
//    Mesh::~Mesh() {
//        std::cout << "Mesh destroyed " << name << '\n';
//    }
    Mesh::Mesh() noexcept: Mesh("") {}
    Mesh::Mesh(std::string && name) noexcept: name(name) {}

    int Mesh::vertice_count() const {return static_cast<int>(vertices.size()); }
    int Mesh::normals_count() const {return static_cast<int>(vertices.size()); }
    int Mesh::texture_count() const {return static_cast<int>(vertices.size()); }
    int Mesh::triangles_count() const {
        return static_cast<int>(std::size(triangles_iterator(faces, vertices)));
    }

}

