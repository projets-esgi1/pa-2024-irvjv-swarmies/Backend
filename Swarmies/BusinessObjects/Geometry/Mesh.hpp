//
// Created by ivo on 01/11/2023.
//

#ifndef SWARMIES_MESH_HPP
#define SWARMIES_MESH_HPP

#include <string>
#include <vector>
#include <array>
#include <map>

#include <cstddef>
#include <cassert>


struct VertexWrapper  {
    float abscisse;
    float ordonnee;
    float prof;
};

struct Vertex {
    int coordinates_ref;
    int normal_ref;
};

/**
 * Fonction qui à partir de deux valeurs entieres petites
 * de 16 bits en retourne la fusion (un entier 32 bits)
 */
typedef int VertexHash;
static inline VertexHash vert_hash(int vert_pos, int norm_idx) {
    assert(vert_pos < (1 << 16));
    assert(norm_idx < (1 << 16));
    return ((vert_pos & ((1 << 16) - 1)) << 16) + (norm_idx & ((1 << 16) - 1));
}

namespace Swarmies {

    /**
     * Stores vertex coordinates
     * vertex normals
     * map of vertices coordinates/normals couples
     * array of faces pointing to these
     */
    struct Mesh {

        std::string name;
        std::vector<std::array<std::array<int, 3>, 3>> faces;
        std::map<VertexHash, Vertex> vertices;
        char variant;

        explicit Mesh() noexcept;
        explicit Mesh(std::string && name) noexcept;
        explicit Mesh(const std::string & name) = delete;

        [[nodiscard]] int vertice_count() const;
        [[nodiscard]] int normals_count() const;
        [[nodiscard]] int texture_count() const;
        [[nodiscard]] int triangles_count() const;

        /**
         * Iterator that yields every vertex from a topology
         * in a way that unity can work with, where the vertex
         * of every face is yielded as many times as needed for every
         * face that uses that vertex
         */
        struct triangles_iterator {
            explicit triangles_iterator(
                    decltype(Mesh::faces) const & faces,
                    decltype(Mesh::vertices) const & vertices
            )
            : faces(faces), verts(vertices), _current_face(0), _current_vertex(0) {}
        private:
            int _current_face;
            int _current_vertex;
            decltype(Mesh::faces) const & faces;
            decltype(Mesh::vertices) const & verts;
        public:
            bool operator!=(const triangles_iterator & other) const {
                return _current_face != other._current_face || _current_vertex != other._current_vertex;
            }

            long operator*() {
                int hash = vert_hash(faces[_current_face][_current_vertex][0], faces[_current_face][_current_vertex][2]);
                /// f 5/1/1 3/2/1 1/3/1
                auto it = verts.find(hash);
                auto index = std::distance(verts.begin(), it);

                return index;
            }

            triangles_iterator & operator++() {
                if(_current_vertex < 2) {
                    _current_vertex++;
                } else {
                    _current_vertex = 0;
                    _current_face++;
                }

                return *this;
            }

            [[nodiscard]] triangles_iterator begin() const {
                return *this;
            }

            [[nodiscard]] triangles_iterator end() const {
                triangles_iterator iter = *this;
                iter._current_face = static_cast<int>(faces.size());
                iter._current_vertex = 0;
                return iter;
            }

            [[nodiscard]] std::size_t size() const {
                return faces.size() * 3;
            }
        };
    };

    /**
     * pack un couplet vertex pos vertex normal en un seul nombre de 32 bits
     * les deux parties etant tronquées a 16 bits. On part du principe
     * qu'un mesh aura en dessous de ~65000 sommets
     */
}

#if SWARMIES_TESTING
void testMeshLoads(const char * path);
#endif

#endif //SWARMIES_MESH_HPP
