//
// C'est l'interface que toutes les "glues"
// doivent implémenter pour interagir avec
// le jeu
//

#ifndef SWARMIES_IPLAYGAME_HPP
#define SWARMIES_IPLAYGAME_HPP

#include <vector>
#include <unordered_map>
#include "../BusinessObjects/Geometry/Mesh.hpp"

class IPlayGame {
    virtual int GluMeshVertexCount(const char* name, const char * mesh) = 0;
    virtual int GluMeshNormalsCount(const char* name, const char * mesh) = 0;
    virtual int GluMeshUVCount(const char* name, const char * mesh) = 0;
    virtual int GluMeshTrianglesCount(const char* name, const char * mesh) = 0;
};

#endif //SWARMIES_IPLAYGAME_HPP
