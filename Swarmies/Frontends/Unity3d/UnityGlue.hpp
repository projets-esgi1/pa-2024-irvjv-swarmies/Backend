//
// Created by ivo on 02/01/2024.
//

#ifndef SOLUTIONSWARMIES_UNITYGLUE_HPP
#define SOLUTIONSWARMIES_UNITYGLUE_HPP

#include "../IPlayGame.hpp"
#include "../../BusinessObjects/Geometry/Mesh.hpp"

class UnityPlayGame: IPlayGame {
public:
    int GluMeshVertexCount(const char* name, const char * mesh) final;
    int GluMeshNormalsCount(const char* name, const char * mesh) final;
    int GluMeshUVCount(const char* name, const char * mesh) final;
    int GluMeshTrianglesCount(const char* name, const char * mesh) final;
};


#endif //SOLUTIONSWARMIES_UNITYGLUE_HPP
