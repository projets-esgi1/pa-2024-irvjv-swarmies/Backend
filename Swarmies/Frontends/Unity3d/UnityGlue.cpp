//
// Created by ivo on 28/10/2023.
//

#ifndef NULL
#define NULL nullptr
#endif
#ifndef Assert
#define Assert(foo)
#endif

#include <iostream>
#include <algorithm>

#include "IUnityInterface.h"
#include "IUnityGraphics.h"
#include "IUnityLog.h"
#include "IUnityEventQueue.h"

#include "UnityGlue.hpp"
#include "../../../Engine/3d/AutodeskObjLoader.hpp"

static IUnityLog* unityLog = nullptr;
static std::unordered_map<std::string, Swarmies::Mesh> loaded_obj;

static void load_obj(const char* name) {
    using namespace std::string_literals;
    Engine::insts_t instances;

    loaded_obj = Engine::AutodeskObjLoader().LoadObj(("Assets/GameAssets/"s + name + ".obj").c_str(), instances);
}


static Swarmies::Mesh load_mesh(const char* name, const char * mesh) {

    using namespace std::string_literals;
    /// note: on s'execute depuis une DLL dans le dossier du jeu
    Engine::insts_t instances;

    return Engine::AutodeskObjLoader().LoadObj(("Assets/GameAssets/"s + name + ".obj").c_str(), instances).at(mesh);
}


static void spread_data(
        const char* obj_name,
        const char * mesh_name,
        float vertices[], float norms[], float uvs[], int tris[],
        int instance_num = -1
        )
{
    using namespace std::string_literals;
    Engine::insts_t instances;
    auto loader = Engine::AutodeskObjLoader();

    auto meshes = loader.LoadObj(("Assets/GameAssets/"s + obj_name + ".obj").c_str(), instances);
    Swarmies::Mesh & mesh = meshes.at(mesh_name);
    if(instance_num > -1) {
        mesh = instances.at(mesh_name).at(instance_num);
    }

    int j = 0;

    for (auto const &[vertex_hash, vertex]: mesh.vertices) {
        vertices[j*3+0] = loader.vertices_pos.at(vertex.coordinates_ref - 1).abscisse;
        vertices[j*3+1] = loader.vertices_pos.at(vertex.coordinates_ref - 1).ordonnee;
        vertices[j*3+2] = loader.vertices_pos.at(vertex.coordinates_ref - 1).prof;
        assert(vertex.normal_ref - 1 >= 0);
        assert(loader.normals.size() > vertex.normal_ref - 1);
        norms[j*3+0] = loader.normals.at(vertex.normal_ref - 1).abscisse;
        norms[j*3+1] = loader.normals.at(vertex.normal_ref - 1).ordonnee;
        norms[j*3+2] = loader.normals.at(vertex.normal_ref - 1).prof;
        j++;
    }
//    for (int i = 0; i < mesh.texture_count(); ++i) {
//        uvs[i*2+0] = loader.uvs.at(i).abscisse;
//        uvs[i*2+1] = loader.uvs.at(i).ordonnee;
//    }

    int i = 0;
    for ( auto tri: Swarmies::Mesh::triangles_iterator(mesh.faces, mesh.vertices)) {

        tris[i++] = (int)tri;
        // dans unity, les face doivent referencer des vertices en partant de zero,
        // dans le format.obj ca part de 1
    }
}


int UnityPlayGame::GluMeshVertexCount(const char* name, const char * mesh) {

    return load_mesh(name, mesh).vertice_count();
}

int UnityPlayGame::GluMeshNormalsCount(const char* name, const char * mesh) {

    return load_mesh(name, mesh).normals_count();
}

int UnityPlayGame::GluMeshUVCount(const char* name, const char * mesh) {

    return load_mesh(name, mesh).texture_count();
}

int UnityPlayGame::GluMeshTrianglesCount(const char* name, const char * mesh) {

    return load_mesh(name, mesh).triangles_count();
}

// Unity plugin load event
extern "C" void UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API
UnityPluginLoad(IUnityInterfaces* unityInterfaces)
{
    auto * graphics = unityInterfaces->Get<IUnityGraphics>();
    unityLog = unityInterfaces->Get<IUnityLog>();

    UNITY_LOG(unityLog, "loded unity glue");
}

extern "C" void UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API
UnityPluginUnload()
{
    UNITY_LOG(unityLog, "unload unity glue");

    unityLog = nullptr;
}

extern "C" UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API float SayHello() {
    UNITY_LOG(unityLog, "say hello from glue");

    return 24.f;
}

extern "C" UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API
bool GluHasMeshTerrain(const char * obj, const char * lookup)
{
    load_obj(obj);
    return std::ranges::any_of(
    loaded_obj,[&lookup](const auto& pair) {return pair.first == lookup;}
    );
//    for (const auto & mesh: loaded_obj) {
//        if(mesh.first == lookup) {
//            return true;
//        }
//    }
//    return false;

}

extern "C" UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API
void GluLoadMesh(const char * obj_name, const char * mesh_name, int instance_num, float vertices[], float norms[], float uvs[], int tris[]) {
    UNITY_LOG(unityLog, obj_name);

    spread_data(obj_name, mesh_name, vertices, norms, uvs, tris, instance_num);

    std::cout << "toto" << std::endl;
    //return 24.f;
}

extern "C" UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API
int GluGetLevelInstancesCount(const char * file)
{
    Engine::insts_t instances;

    using namespace std::string_literals;
    auto meshes = Engine::AutodeskObjLoader().LoadObj(("Assets/GameAssets/"s + file + ".obj").c_str(), instances);

    return (int)instances.size();
}

extern "C" UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API
void GluGetLevelInstanceName(const char * file, int i, char iname[])
{
    Engine::insts_t instances;

    using namespace std::string_literals;
    auto meshes = Engine::AutodeskObjLoader().LoadObj(("Assets/GameAssets/"s + file + ".obj").c_str(), instances);

    int c = 0;
    for (const auto & [name, mesh]: instances) {
        if(c++ != i)
            continue;
        strlcpy(iname, name.data(), 256);
        break;
    }
}

extern "C" UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API
int GluGetLevelInstanceNameCount(const char * file, const char * name)
{
    Engine::insts_t instances;

    using namespace std::string_literals;
    Engine::AutodeskObjLoader().LoadObj(("Assets/GameAssets/"s + file + ".obj").c_str(), instances);

    try {
        return (int)instances.at(name).size();
    } catch (std::out_of_range&) {
        return -1;
    }
}

extern "C" UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API
int GluLoadLevelInstances(const char * name, const char * mesh)
{
    Engine::insts_t instances;

    using namespace std::string_literals;
    auto meshes = Engine::AutodeskObjLoader().LoadObj(("Assets/GameAssets/"s + name + ".obj").c_str(), instances);

    return 0;
}

extern "C" UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API
int GluMeshVerticesNumber(const char * name, const char * mesh) {
    UNITY_LOG(unityLog, name);

    return (UnityPlayGame().GluMeshVertexCount(name, mesh));
}

extern "C" UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API
int GluMeshNormalsNumber(const char * name, const char * mesh) {
    UNITY_LOG(unityLog, name);

    return (UnityPlayGame().GluMeshNormalsCount(name, mesh));
}

extern "C" UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API
int GluMeshUVNumber(const char * name, const char * mesh) {
    UNITY_LOG(unityLog, name);

    return (UnityPlayGame().GluMeshUVCount(name, mesh));
}

extern "C" UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API
int GluMeshTrianglesNumber(const char * name, const char * mesh) {
    UNITY_LOG(unityLog, name);
    auto num = (UnityPlayGame().GluMeshTrianglesCount(name, mesh));
    return num;
}

struct MyCustomEvent {};

// see https://makaka.org/unity-tutorials/guid
// https://docs.unity.com/ugs/manual/analytics/manual/event-manager
// https://forum.unity.com/threads/use-iunityeventqueue-from-a-native-plugin-to-call-send-a-message-to-c.1289540/
REGISTER_EVENT_ID(0xda75015ee8eb4719, 0xaa97c4855661637e, MyCustomEvent);
