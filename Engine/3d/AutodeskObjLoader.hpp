//
// Created by ivo on 27/01/2024.
//

#ifndef SOLUTIONSWARMIES_AUTODESKOBJLOADER_HPP
#define SOLUTIONSWARMIES_AUTODESKOBJLOADER_HPP

#include <unordered_map>
#include <string>
#include "../../Swarmies/BusinessObjects/Geometry/Mesh.hpp"

namespace Engine {

    typedef std::map<std::string , std::vector<Swarmies::Mesh>> insts_t;

    class AutodeskObjLoader {
        std::unordered_map<std::string, Swarmies::Mesh&> objects;
    public:
        std::vector<struct VertexWrapper> vertices_pos;
        std::vector<struct VertexWrapper> normals;
        std::vector<struct VertexWrapper> uvs;

        Swarmies::Mesh LoadMesh(std::FILE * file);
        std::unordered_map<std::string, Swarmies::Mesh> LoadObj(const char *, insts_t &);
    };

} // Engine

#if SWARMIES_TESTING
void testMultiMesh();
#endif

#endif //SOLUTIONSWARMIES_AUTODESKOBJLOADER_HPP
