//
// Created by ivo on 27/01/2024.
//

#include "AutodeskObjLoader.hpp"

namespace Engine {

    auto AutodeskObjLoader::LoadObj(
        const char *path, insts_t & instances
    )-> std::unordered_map<std::string, Swarmies::Mesh> {
        auto meshes = std::unordered_map<std::string, Swarmies::Mesh>();

        using namespace std::string_literals;
        std::FILE * file = std::fopen( path, "rb");

        while (!std::feof(file)) {
            const auto & mesh = LoadMesh(file);

            char template_name[246];
            int inst_num;
            char variant;
            int found = sscanf(mesh.name.c_str(), "%[^.].%3d.%c", template_name, &inst_num, &variant);
            if(found >= 2)
                printf("found an instance of %s %d - ", template_name, inst_num);
            if(found == 3) {

            } else if(found == 2) {
                instances[template_name].push_back(mesh);
            } else {
                meshes.emplace(mesh.name, mesh);
            }
        }

        return meshes;
    } // AutodeskObjLoader::LoadObj


    /**
 * Charge un mesh depuis un fichier .obj dans un format utilisable
 * par Unity (meme principe que Assimp). Charge le premier
 * mesh qu'il trouve
 */
    auto AutodeskObjLoader::LoadMesh(
            std::FILE * file
    ) -> Swarmies::Mesh {
        char buf[256];
        char name[256] =  {0};
        Swarmies::Mesh mesh("*replace*");

        while (std::fgets(buf, sizeof buf, file) != nullptr) {

            if(buf[0] == 'v') {
                struct VertexWrapper vw{0, 0, 0};
                switch (buf[1]) {
                    case ' ':
                        sscanf(buf, "v %f %f %f\n", &vw.abscisse, &vw.ordonnee, &vw.prof);
                        vertices_pos.push_back(vw);
                        break;
                    case 'n':
                        sscanf(buf, "vn %f %f %f\n", &vw.abscisse, &vw.ordonnee, &vw.prof);
                        normals.push_back(vw);
                        break;
                    case 't':
                        sscanf(buf, "vt %f %f\n", &vw.abscisse, &vw.ordonnee);
                        uvs.push_back(vw);
                        break;
                }
            }
            else if(buf[0] == 'f') {
                auto a = std::array{std::array{0, 0, 0}, std::array{0, 0, 0}, std::array{0, 0, 0}};
                sscanf(buf, "f %d/%d/%d %d/%d/%d %d/%d/%d",
                       &a[0][0], &a[0][1], &a[0][2], &a[1][0], &a[1][1], &a[1][2], &a[2][0], &a[2][1], &a[2][2]);
                mesh.faces.push_back(a);
                for (int i = 0; i < 3; ++i) {
                    assert(a[i][0] - 1 < vertices_pos.size());
                    assert(a[i][2] - 1 < normals.size());
                    mesh.vertices[vert_hash(a[i][0], a[i][2])]
                        = Vertex{a[i][0], a[i][2]};
                }
            }
            /// quand on a un fichier où la ligne courante lue contient notre nom
            /// ou alors un fichier où on passe de l'entete
            if(strlen(name) == 0) {
                if(buf[0] == 'o') {
                    sscanf(buf, "o %255[^\n]", name);
                    continue;
                }
            } else { /// if strlen(name) > 0
                int c;
                if((c = std::getc(file)) == 'o') {
                    std::ungetc('o', file);
                    break;
                }
                std::ungetc(c, file);
                /// si on tombe sur une ligne "o Bonhomme", quand on parse
                /// un mesh, on a fini de parser ce mesh
            }
        }
        mesh.name = name;
        return mesh;
        //return {name, old.vertices_pos, old.normals, old.uvs, old.faces, old.vertices};
    }
} // ::Engine

#if SWARMIES_TESTING
#include <cstdio>
#include <cassert>
#include <iostream>

void testMultiMesh() {
    {
        Engine::insts instances;
        auto meshes = Engine::AutodeskObjLoader().LoadObj("Assets/test_topo3.obj", instances);
        assert(!meshes.empty());
        assert(meshes.size() == 3);
        assert(meshes["Terrain"].name == "Terrain");
        assert(meshes["Bonhomme"].name == "Bonhomme");
        assert(meshes["Bataille"].name == "Bataille");
    }
    {
        Engine::insts instances;

        auto meshes = Engine::AutodeskObjLoader().LoadObj("Assets/bataille1.obj", instances);
        assert(!meshes.empty());
        assert(meshes.size() == 2);
        assert(meshes["Terrain"].name == "Terrain");
        assert(meshes["Bonhomme"].name == "Bonhomme");
        assert(instances.size() == 1);
        assert(instances["Bonhomme"].size() == 16);
    }
    puts("testMultiMesh works");
}

void testMeshLoads(const char * path) {
    std::FILE * file = std::fopen(path, "rb");

    if(file == nullptr) {
        std::cerr << (std::string("Can't open ") + path + ".").c_str();
        assert(0 && "Can't open asset file");
    }

    auto load = Engine::AutodeskObjLoader();
    auto mesh = load.LoadMesh(file);
    assert(!mesh.faces.empty());
    assert(load.vertices_pos.size() == 54);
    assert(load.normals.size() == 6);
    assert(load.uvs.size() == 14);
    assert(mesh.faces.size() == 100);
    assert(mesh.vertices.size() == 144);

    long i =0;
    for(auto f: Swarmies::Mesh::triangles_iterator(mesh.faces, mesh.vertices)) {
        i = f;
    }

    assert(i ==135);
    puts("testMeshLoads ok");
}

#endif